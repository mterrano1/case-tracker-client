# Boilerplate---GitLab CI/CD Pipeline for a React App

This is a sample GitLab CI/CD pipeline for a default React app that includes a Dockerfile and .gitlab-ci.yml file. The pipeline is designed to build, test, and deploy the React app using Docker containers.

## How it works

The pipeline consists of three stages:

### Build

In the build stage, the Docker image for the React app is built using the Dockerfile. The image is built from a base image of Node.js version 16 and the necessary packages are installed using the package.json and package-lock.json files. The app code is then copied to the image and built using the `npm run build` command.

### Test

In the test stage, the Docker image is run in a container and the tests for the React app are run using the `npm test` command.

### Deploy

In the deploy stage, the Docker image is pushed to a Docker registry using the `docker push` command. The Docker registry credentials are passed using environment variables, which are set in the GitLab project settings.

## Installing a GitLab Runner

1. Install the GitLab runner binary by following the instructions [here](https://docs.gitlab.com/runner/install/).
2. Register the runner by running the following command: `sudo gitlab-runner register`.
3. Follow the prompts to configure the runner, including specifying the GitLab instance URL and the registration token. You can find the registration token in your GitLab project's settings under CI/CD > Runners.
4. Choose the executor type as `docker`, and specify the default image to be used for your jobs. For example, you can specify `node:16` to use the Node.js version 16 image.
5. Start the runner with the following command: `sudo gitlab-runner start`.

Once you have installed and registered the GitLab runner, you can start using this pipeline by copying the Dockerfile and .gitlab-ci.yml files to your project directory and modifying them as needed.

## How to use

To use this pipeline for your own React app, you can follow these steps:

1. Copy the Dockerfile and .gitlab-ci.yml files to your project directory.
2. Modify the Dockerfile to include the necessary packages and commands for your React app.
3. Modify the .gitlab-ci.yml file to include the appropriate image names and credentials for your Docker registry.
4. Commit and push your changes to your GitLab repository.
5. The pipeline will automatically start building, testing, and deploying your React app using Docker containers.

## Conclusion

This pipeline demonstrates how to use GitLab CI/CD with Docker containers to build, test, and deploy a React app. By using Docker containers, the pipeline provides a portable and consistent environment for running the app, which can simplify the deployment process and make it easier to manage dependencies.
